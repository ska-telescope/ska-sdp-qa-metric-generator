include .make/base.mk
include .make/oci.mk
include .make/python.mk
include .make/dependencies.mk

PYTHON_LINE_LENGTH = 99

.PHONY: build run processor-help shell

MS ?= data/61MB.ms
PROCESSOR = ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics
OPTS ?=

CASACORE_MEASURES_DIR ?= /usr/share/casacore/data

PYTHON_VARS_AFTER_PYTEST = --cov-fail-under=90 --no-cov-on-fail --durations=10 -vv

# We need casacore data
python-pre-test:
	./install_measures.sh "$(CASACORE_MEASURES_DIR)"


build:
	docker build -t signal-generator .

run:
	docker run --network=ska-sdp-qa-metric-network -it \
		-v $$(pwd)/data:/data \
		-e SDP_CONFIG_HOST=etcd \
		-e SDP_PB_ID=pb-testrealtime-20240913-95421 \
		-e SDP_EB_ID=eb-test-20240913-95421 \
		-e SDP_KAFKA_HOST=broker \
		--shm-size 2gb\
		signal-generator \
		bash -c "plasma-processor ${PROCESSOR} --metrics all --input /${MS} ${OPTS}"

processor-help:
	docker run --network=ska-sdp-qa-metric-network -it \
		-v $$(pwd)/data:/data \
		-e SDP_CONFIG_HOST=etcd \
		-e SDP_PB_ID=pb-testrealtime-20240913-95421 \
		-e SDP_EB_ID=eb-test-20240913-95421 \
		-e SDP_KAFKA_HOST=broker \
		signal-generator \
		bash -c "plasma-processor ${PROCESSOR} --help"

shell:
	docker run -it \
		-v $$(pwd)/data:/data \
		-e SDP_CONFIG_HOST=etcd \
		-e SDP_PB_ID=pb-testrealtime-20240913-95421 \
		-e SDP_EB_ID=eb-test-20240913-95421 \
		-e SDP_KAFKA_HOST=broker \
		--shm-size 2gb\
		signal-generator \
		bash
