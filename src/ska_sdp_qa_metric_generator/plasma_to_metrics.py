"""Class to convert data from Plasma to the Signal Metrics API.

This class is meant to be run with the Plasma receivers, and as such requires
that the plasma store is operational, as well as data is written to it in the
correct way.
"""

import logging

from overrides import override
from ska_sdp_datamodels.visibility import Visibility

from ska_sdp_qa_metric_generator.metrics import metrics_to_dict, process_metrics
from ska_sdp_qa_metric_generator.plasma_common_processor import SignalDisplayCommon
from ska_sdp_qa_metric_generator.utils.config import get_metrics_list
from ska_sdp_qa_metric_generator.utils.utilities import Timer
from ska_sdp_qa_metric_generator.utils.visibility import VisibilityHelper

logger = logging.getLogger(__name__)


class SignalDisplayMetrics(SignalDisplayCommon):
    """Processer to send data to the signal displays."""

    def __init__(
        self,
        random_ids=False,
        ignore_config_db=False,
        disable_kafka=False,
        metrics: list[str] = None,
    ):
        if metrics is None:
            metrics = ["stats"]

        super().__init__(
            random_ids,
            ignore_config_db,
            disable_kafka,
            "all" in metrics or "stats" in metrics,
        )

        self.metrics = get_metrics_list(metrics)

        self._flow_watcher(self.metrics)

    @staticmethod
    @override
    def create(argv) -> "SignalDisplayMetrics":  # pragma: no cover
        """Create instance of SignalDisplayMetrics."""
        parser = SignalDisplayCommon.create_parser("SignalDisplayMetrics", "")
        parser.add_argument(
            "--metrics",
            type=str,
            help="Comma seperated list of metrics",
            default="stats",
        )
        args = parser.parse_args(argv)

        return SignalDisplayMetrics(
            random_ids=args.use_random_ids,
            ignore_config_db=args.ignore_config_db,
            disable_kafka=args.disable_kafka,
            metrics=args.metrics.split(","),
        )

    async def _process_dataset(self, dataset: Visibility):
        local_dataset = VisibilityHelper(
            dataset,
            self._processing_block_id,
            self._subarray_id,
            self._execution_block_id,
        )

        send = process_metrics(self.metrics, self.flows, local_dataset)

        with Timer(logger, "Send all data"):
            for flow, outputs in send:
                for partition, value in enumerate(outputs):
                    await self._producer.send(
                        data=metrics_to_dict(value),
                        topic=flow.topic,
                        partition=partition,
                    )


def main():  # pragma: no cover
    """A do nothing main."""
    print("This file should not be run on it's own, an example to run this file would be:")
    print(
        "plasma-processor "
        "ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics "
        "--plasma_socket /plasma/socket "
        "--readiness-file /tmp/processor_ready"
        "--metrics all"
    )


if __name__ == "__main__":  # pragma: no cover
    main()
