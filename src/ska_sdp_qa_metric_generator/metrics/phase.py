"""The calculations required for the Phase graphs."""

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataAndComponentPayload,
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
)

from ska_sdp_qa_metric_generator.utils.config import SPECTRUM_MAX, SPECTRUM_MIN
from ska_sdp_qa_metric_generator.utils.flow import SignalDisplayFlow
from ska_sdp_qa_metric_generator.utils.visibility import VisibilityHelper

# pylint: disable=duplicate-code,too-many-locals,too-many-function-args


def phase(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """Phase graph calculation"""
    return [
        (
            flow,
            [_process_phase_data(data.values, dataset, window, flow) for window in flow.windows()],
        )
        for data in dataset.data
        for flow in flows
    ]


def _process_phase_data(
    data: np.ndarray,
    dataset: VisibilityHelper,
    window: tuple[int, int, int],
    flow: SignalDisplayFlow,
):
    spectrum_start, spectrum_end, nchan_avg = window
    baselines, _, _ = dataset.baselines
    polarisations = dataset.polarisations

    payload = MetricPayload(
        data_type=MetricDataTypes.PHASE,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(**dataset.spectral_window),
    )

    if nchan_avg > 1:

        num_rows, num_columns, num_polarisations = data.shape
        padded_size = (
            num_columns + (nchan_avg - num_columns % nchan_avg) % nchan_avg
        )  # Size after padding

        padded_data = np.pad(
            data.astype(complex),
            ((0, 0), (0, padded_size - num_columns), (0, 0)),
            mode="constant",
            constant_values=np.NaN,
        )

        reshaped_data = padded_data.reshape(num_rows, -1, nchan_avg, num_polarisations)
        data = np.nanmean(reshaped_data, axis=2)

    start_index, end_index = dataset.spectrum_indexes(spectrum_start, spectrum_end, data.shape[1])

    angles = np.round(np.angle(data[:, start_index:end_index, :]), flow.rounding_sensitivity)
    imag_components = np.round(
        np.imag(data[:, start_index:end_index, :]), flow.rounding_sensitivity
    )

    payload.spectral_window.count = angles.shape[1]
    if not (spectrum_start == SPECTRUM_MIN and spectrum_end == SPECTRUM_MAX):
        payload.spectral_window.freq_min = spectrum_start
        payload.spectral_window.freq_max = spectrum_end

    payload.data = [
        DataAndComponentPayload(
            baseline=baseline,
            polarisation=polarisation,
            data=angles[baseline_index, :, polarisation_index].tolist(),
            component=imag_components[baseline_index, :, polarisation_index].tolist(),
        )
        for baseline_index, baseline in enumerate(baselines)
        for polarisation_index, polarisation in enumerate(polarisations)
    ]

    return payload
