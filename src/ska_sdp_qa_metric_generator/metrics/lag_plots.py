"""The calculations for the Lag Plots."""

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataPayload,
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
)

from ska_sdp_qa_metric_generator.utils.config import SPECTRUM_MAX, SPECTRUM_MIN
from ska_sdp_qa_metric_generator.utils.flow import SignalDisplayFlow
from ska_sdp_qa_metric_generator.utils.visibility import VisibilityHelper

# pylint: disable=duplicate-code,too-many-locals,too-many-function-args


def lag_plots(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """The Lag Plot calculation."""
    return [
        (
            flow,
            [_process_lag_plots_data(data.values, dataset, window) for window in flow.windows()],
        )
        for data in dataset.data
        for flow in flows
    ]


def _process_lag_plots_data(
    data: np.ndarray, dataset: VisibilityHelper, window: tuple[int, int, int]
):
    """Generate plot of cross-correlation power vs lag."""
    spectrum_start, spectrum_end, nchan_avg = window
    baselines, _, _ = dataset.baselines
    polarisations = dataset.polarisations

    payload = MetricPayload(
        data_type=MetricDataTypes.LAG_PLOT,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(**dataset.spectral_window),
    )

    if nchan_avg > 1:
        num_rows, num_columns, num_polarisations = data.shape
        padded_size = num_columns + (nchan_avg - num_columns % nchan_avg) % nchan_avg

        padded_data = np.pad(
            data.astype(complex),
            ((0, 0), (0, padded_size - num_columns), (0, 0)),
            mode="constant",
            constant_values=np.NaN,
        )
        reshaped_data = padded_data.reshape(num_rows, -1, nchan_avg, num_polarisations)
        data = np.nanmean(reshaped_data, axis=2)

    start_index, end_index = dataset.spectrum_indexes(spectrum_start, spectrum_end, data.shape[1])

    data = data[:, start_index:end_index, :]

    # Filter out autocorrelations
    valid_baseline_mask = np.array([a1 != a2 for a1, a2 in (b.split("_") for b in baselines)])
    data = data[valid_baseline_mask]  # Only use cross-correlation data
    valid_baselines = [b for i, b in enumerate(baselines) if valid_baseline_mask[i]]

    fft_data = np.abs(np.fft.fftshift(np.fft.ifft(data, axis=1), axes=1))
    fft_data /= np.max(fft_data, axis=1, keepdims=True)
    fft_data *= 360

    payload.spectral_window.count = fft_data.shape[1]
    if not (spectrum_start == SPECTRUM_MIN and spectrum_end == SPECTRUM_MAX):
        payload.spectral_window.freq_min = spectrum_start
        payload.spectral_window.freq_max = spectrum_end

    for baseline_idx, baseline in enumerate(valid_baselines):
        for pol_idx, polarisation in enumerate(polarisations):
            payload.data.append(
                DataPayload(
                    baseline=baseline,
                    polarisation=polarisation,
                    data=fft_data[baseline_idx, :, pol_idx].tolist(),
                )
            )

    return payload
