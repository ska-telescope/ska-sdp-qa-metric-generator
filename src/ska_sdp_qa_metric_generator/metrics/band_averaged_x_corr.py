"""The calculations for the Lag Plots."""

# pylint: disable=duplicate-code,too-many-locals,too-many-function-args

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataPayload,
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
)

from ska_sdp_qa_metric_generator.utils.flow import SignalDisplayFlow
from ska_sdp_qa_metric_generator.utils.visibility import VisibilityHelper


def band_averaged_x_corr(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """The Lag Plot calculation."""
    return [
        (flow, [_process_band_averaged_x_corr_data(data.values, dataset)])
        for data in dataset.data
        for flow in flows
    ]


def _process_band_averaged_x_corr_data(
    data: np.ndarray,
    dataset: VisibilityHelper,
):
    """Generate plot of cross-corellation power vs lag.

    :param data: visibility data array
    :param baselines: baselines
    :param polarisation: polarisations
    :param processing_block: The data block to be processed
    """
    baselines, _, _ = dataset.baselines
    polarisations = dataset.polarisations

    payload = MetricPayload(
        data_type=MetricDataTypes.BAND_AVERAGED_X_CORR,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(**dataset.spectral_window),
    )

    for baseline_index, baseline in enumerate(baselines):
        for polarisation_index, polarisation in enumerate(polarisations):
            visibilities = data[baseline_index, :, polarisation_index]
            x_corr_power = np.abs(np.square(np.mean(visibilities)))
            payload.data.append(
                DataPayload(
                    baseline=baseline,
                    polarisation=polarisation,
                    data=x_corr_power.tolist(),
                )
            )

    return payload
