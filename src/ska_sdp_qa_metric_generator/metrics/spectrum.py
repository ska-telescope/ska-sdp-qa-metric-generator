"""The calculation for the Spectrum plots"""

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
    SpectrumPayload,
)

from ska_sdp_qa_metric_generator.utils.config import SPECTRUM_MAX, SPECTRUM_MIN
from ska_sdp_qa_metric_generator.utils.flow import SignalDisplayFlow
from ska_sdp_qa_metric_generator.utils.visibility import VisibilityHelper

# pylint: disable=duplicate-code,too-many-locals,too-many-function-args


def spectrum(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """The calculation for the Spectrum Plots"""
    return [
        (
            flow,
            [
                _process_spectrum_data(data.values, dataset, window, flow)
                for window in flow.windows()
            ],
        )
        for data in dataset.data
        for flow in flows
    ]


def _process_spectrum_data(
    data, dataset: VisibilityHelper, window: tuple[int, int, int], flow: SignalDisplayFlow
):
    spectrum_start, spectrum_end, nchan_avg = window
    _, antenna1, antenna2 = dataset.baselines

    auto = np.array(antenna1) == np.array(antenna2)
    data = data[auto, :, :]  # Filter data for autocorrelations

    payload = MetricPayload(
        data_type=MetricDataTypes.SPECTRUM,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(**dataset.spectral_window),
    )

    if nchan_avg > 1:
        num_rows, num_columns, num_polarisations = data.shape
        padded_size = num_columns + (nchan_avg - num_columns % nchan_avg) % nchan_avg

        padded_data = np.pad(
            data,
            ((0, 0), (0, padded_size - num_columns), (0, 0)),
            mode="constant",
            constant_values=np.NaN,
        )
        reshaped_data = padded_data.reshape(num_rows, -1, nchan_avg, num_polarisations)
        data = np.nanmean(reshaped_data, axis=2)

    start_index, end_index = dataset.spectrum_indexes(spectrum_start, spectrum_end, data.shape[1])

    payload.spectral_window.count = data.shape[1]
    if not (spectrum_start == SPECTRUM_MIN and spectrum_end == SPECTRUM_MAX):
        payload.spectral_window.freq_min = spectrum_start
        payload.spectral_window.freq_max = spectrum_end

    spectrum_mean = np.mean(data[:, start_index:end_index, :], axis=0)

    magnitude = np.abs(spectrum_mean)
    angle = np.angle(spectrum_mean)

    payload.spectral_window.count = magnitude.shape[0]

    payload.data = [
        SpectrumPayload(
            polarisation=pol,
            power=np.round(magnitude[:, idx], flow.rounding_sensitivity).tolist(),
            angle=np.round(angle[:, idx], flow.rounding_sensitivity).tolist(),
        )
        for idx, pol in enumerate(dataset.polarisations)
    ]

    return payload
