"""Class to convert data from Plasma to the Signal Metrics API.

This class is meant to be run with the Plasma receivers, and as such requires
that the plasma store is operational, as well as data is written to it in the
correct way.
"""

# pylint: disable=unused-argument,too-many-instance-attributes,too-many-locals
# pylint: disable=broad-except,c-extension-no-member,protected-access
# pylint: disable=too-many-return-statements

import argparse
import concurrent.futures
import logging
import os
import threading
from dataclasses import asdict
from time import time
from typing import Any

import numpy
import ska_sdp_config
from overrides import override
from realtime.receive.processors.sdp.base_processor import BaseProcessor
from ska_sdp_datamodels.visibility import Visibility
from ska_sdp_dataqueues import DataQueueProducer, Encoding
from ska_sdp_dataqueues.schemas.signal_display_metrics import VisReceiveStatistics

from ska_sdp_qa_metric_generator.utils.flow import SignalDisplayFlow
from ska_sdp_qa_metric_generator.utils.utilities import Timer, create_id

logger = logging.getLogger(__name__)


class SignalDisplayCommon(BaseProcessor):
    """Processer to send data to the signal displays."""

    def __init__(
        self,
        random_ids=False,
        ignore_config_db=False,
        disable_kafka=False,
        enable_stats=True,
    ):
        super().__init__()
        logger.warning("Starting...")

        self.enable_stats = enable_stats
        self.received_payloads = 0
        self.received_time_slices = 0
        self.process_pollrate = 1.0
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.flows = []

        self._processing_block_id = os.environ.get("SDP_PB_ID", "pb-unknown")
        self._execution_block_id = os.environ.get("SDP_EB_ID", "eb-unknown")
        self._subarray_id = os.environ.get("SDP_SUBARRAY_ID", "01")
        self._scan_id = 0
        self._disable_kafka = disable_kafka
        self._ignore_config_db = ignore_config_db
        self.config = None
        self.first_scan = True

        if self._disable_kafka:
            logger.warning("Kafka is disabled")
            self._producer = None
        else:
            broker_instance = os.environ.get("SDP_KAFKA_HOST")
            if not broker_instance:
                raise ValueError("ENV 'SDP_KAFKA_HOST' is not set")
            self._producer = DataQueueProducer(
                server=broker_instance,
                message_max_bytes=500 * 1024 * 1024,
                encoding=Encoding.MSGPACK_NUMPY,
            )

        self.time_since_last_payload = time()

        if random_ids:
            self._processing_block_id = create_id("pb", "fake")
            self._execution_block_id = create_id("eb", "fake")
        elif not ignore_config_db and self._processing_block_id != "pb-unknown":
            self.config = ska_sdp_config.Config()
            for txn in self.config.txn():
                processing_block = txn.processing_block.get(self._processing_block_id)
                self._execution_block_id = processing_block.eb_id
                execution_block = txn.execution_block.get(self._execution_block_id)
                self._subarray_id = execution_block.subarray_id

        logger.info("Processing Block: %s", self._processing_block_id)
        logger.info("Execution Block: %s", self._execution_block_id)
        logger.info("Subarray: %s", self._subarray_id)
        logger.info("Topics configured:")

    @staticmethod
    def create_parser(name="SignalDisplay", description=""):  # pragma: no cover
        """Create instance of SignalDisplayMetrics."""
        parser = argparse.ArgumentParser(
            description=description,
            prog=name,
        )
        parser.add_argument(
            "--use-random-ids",
            action="store_true",
            help="Whether to use random IDs or not",
        )
        parser.add_argument(
            "--ignore-config-db",
            action="store_true",
            help="Whether to ignore the config DB for the IDs",
        )
        parser.add_argument(
            "--disable-kafka",
            action="store_true",
            help="Do not send data to Kafka",
        )
        return parser

    @override
    async def process(self, dataset: Visibility) -> None:
        """
        Processes the given visibilities dataset.

        :param dataset: A dataset read from Plasma.
        :returns: True to stop processing, false to continue reading payloads.
        """

        logger.info("Received dataset")

        # Log error, but ignore it
        try:
            if self.first_scan:
                await self._producer._producer_start()
                await self._send_updates("start")
                self.first_scan = False

            self.received_payloads += 1
            self.received_time_slices += len(dataset.coords["time"].values)
            with Timer(logger, "Process Dataset"):
                await self._process_dataset(dataset)
            if self.enable_stats:
                with Timer(logger, "Send Stats"):
                    await self._send_updates("receiving")

            self.time_since_last_payload = time()
        except Exception as exc:  # pragma: no cover
            logger.exception(exc)

    async def start_scan(self, scan_id: int) -> None:
        """
        Called when a new scan has started. The default implementation ignores
        this event, but subclasses might want to react to this.

        :param scan_id: the ID of the scan that has started.
        """
        if isinstance(scan_id, numpy.int64):
            scan_id = scan_id.item()
        self._scan_id = scan_id
        await self._send_updates("new")
        self._finish_writing()

    async def end_scan(self, scan_id: int) -> Any:
        """
        Called when a scan has ended. The default implementation ignores this
        event, but subclasses might want to react to this.

        :param scan_id: the ID of the scan that has ended.
        """
        if isinstance(scan_id, numpy.int64):
            scan_id = scan_id.item()
        self._scan_id = scan_id
        await self._send_updates("end_scan")
        self._finish_writing()

    def _finish_writing(self):  # pragma: no cover
        self.received_payloads = 0
        self.received_time_slices = 0
        self.time_since_last_payload = time()

    def _flow_watcher(self, metrics):
        self.flows = self._get_flows(metrics)
        if self._ignore_config_db:
            return
        threading.Thread(target=self._flow_thread, args=(metrics,)).start()

    def _flow_thread(self, metrics):
        for watcher in self.config.watcher():
            for txn in watcher.txn():
                for _, flows in self.flows.items():
                    for flow in flows:
                        state = txn.flow.state(flow.flow).get()
                        flow.state = state

    def _get_flows(self, metrics):
        if self._ignore_config_db:
            return {
                metric: [
                    SignalDisplayFlow(
                        name=f"flow-{metric}",
                        topic=f"metrics-{metric}-{self._subarray_id}",
                        host="",
                        data_format="msgpack_numpy",
                        metric_type=metric,
                        nchan_avg=1,
                        additional_windows=0,
                        rounding_sensitivity=5,
                        flow=None,
                        state=None,
                    )
                ]
                for metric in metrics
            }

        if self.config is None:
            self.config = ska_sdp_config.Config()
        flows = {}
        # pylint: disable-next=too-many-nested-blocks
        for txn in self.config.txn():
            for flow in txn.flow.list_keys():
                if flow.pb_id == self._processing_block_id and flow.kind == "data-queue":
                    config_flow = txn.flow.get(flow)

                    for source in config_flow.sources:
                        if (
                            source.function
                            != "ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics"
                        ):
                            continue
                        if source.parameters["metric_type"] in metrics:
                            signal_display_flow = SignalDisplayFlow(
                                name=config_flow.key.name,
                                topic=config_flow.sink.topics,
                                host=config_flow.sink.host,
                                data_format=config_flow.sink.format,
                                metric_type=source.parameters["metric_type"],
                                nchan_avg=source.parameters["nchan_avg"],
                                additional_windows=source.parameters.get("additional_windows", 0),
                                rounding_sensitivity=source.parameters.get(
                                    "rounding_sensitivity", 5
                                ),
                                flow=config_flow,
                                state=None,
                            )
                            if signal_display_flow.metric_type in flows:
                                flows[signal_display_flow.metric_type].append(signal_display_flow)
                            else:
                                flows[signal_display_flow.metric_type] = [signal_display_flow]
        return flows

    @override
    async def close(self):
        """Signal this processor to stop its activity and return"""
        self._scan_id = 0
        await self._send_updates("stopped")
        if self._disable_kafka:
            logger.info("Skipping flush on disabled kafka")  # pragma: no cover
        else:
            await self._producer._producer_stop()

    async def _process_dataset(self, dataset: Visibility):  # pragma: no cover
        pass

    async def _send_updates(self, state):
        if self._disable_kafka or not self.enable_stats:
            return

        try:
            data = VisReceiveStatistics(
                time=time(),
                type="visibility_receive",
                state=state,
                processing_block_id=self._processing_block_id,
                execution_block_id=self._execution_block_id,
                subarray_id=self._subarray_id,
                scan_id=self._scan_id,
                payloads_received=self.received_payloads,
                time_slices=self.received_time_slices,
                time_since_last_payload=time() - self.time_since_last_payload,
            )
            await self._producer.send(
                data=asdict(data),
                encoding=Encoding.JSON,
                topic=f"metrics-stats-{self._subarray_id}",
            )
        except Exception as error:  # pragma: no cover
            logger.exception(error)
