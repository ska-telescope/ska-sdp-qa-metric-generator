# Overview

The metric-generator project is for generating different Signal metrics.

More information can be found in the [Read The Docs](https://developer.skao.int/projects/ska-sdp-qa-data-api/en/latest/index.html) page.

For more information on the generator you can either check the `docs/` folder
or go to [Read The Docs](https://developer.skao.int/projects/ska-sdp-qa-metric-generator/en/latest/index.html).

## Getting Started

This repo cannot run on it's own in Docker. And requires that the `ska-sdp-qa-data-api`
project be started up first.

To run this application use `make build`, and then `make run`.

You should also be able to run all the tools in the host machine as long as you
set the Kafka connection string first.

There is also a casacore dependency to install some data files. Have a look at
the `Dockerfile` for the curl command.

## Generate Metrics

Use the command `make run` to generate the metrics. You can use any file in
the `data/` directory that has an extention of `.ms` or `.MS` and can be
specified using `make run MS=data/file.ms`

The generator can either be used from Plasma data, or from a Measurement set.

By default if you use the Makefile commands, only measurement set file are used.
The plasma connection is generally only used in integration tests, or in full
system runs.

### Plasma Connection

If you have access to a Plasma connection then use:

```bash
SDP_KAFKA_HOST=broker:29092 plasma-processor \
    ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics \
    --plasma_socket /plasma/socket \
    --readiness-file /tmp/processor_ready
```

### Measurement Set

If you want to run from a MS file use:

```bash
SDP_KAFKA_HOST=localhost:9092 plasma-processor \
    ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics \
    --input /path/to/data.ms
```
