# Changelog

## Development

- [Change] Update datastructures to use pydantic.dataclasses
- [Removed] Removed unused values in `spectrum` metrics

## 0.24.0

- [Change] Update structure of config DB flow entries
- [New] Add high resolution code
- [New] Add new partitions for the high resolution code
- [New] Add watchers for flow updates
- [Update] Use new dataqueues library
- [New] Adds configurable rounding options

## 0.23.0

- [Change] Removal of our custom Producer, and switch to using the
  `DataQueueMultiTopic` in the `ska_sdp_dataqueues` module.
- [Change] [Breaking] Use new `stats` topic for the receiver stats.

## 0.22.2

- [New] Add down-scaled data
- [New] Base all data flows on Flows in config DB
- [Removed] [Breaking] All default or non-configDB data removed
- [New] Start/End scans will now come from base class, instead of guessing
- [New] Send topics are now based on flows (except stats)
- [Change] Correct reporting of nchan in spectral window
- [Change] Use nchan_avg over scale_factor for low_res data calcs

## 0.21.0

- [Change] Switch to using the ska-sdp-dataqueues library
- [Change] Send Real & Imaginary parts for the phase and amplitude graphs
- [New] Add new Weight Distribution plots
- [New] Add Band Averaged cross correlation as a function of time graphs
- [Change] Fix the phase calculation to use `atan2`
- [Change] Filter out lag plot baselines corresponding to an autocorrelation