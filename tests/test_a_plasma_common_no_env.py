"""Test the env ValueError.

This file is named badly, it is so it runs first."""

# pylint: disable=unused-argument

from unittest.mock import patch

import pytest

from .test_plasma_common import SignalDisplayFake


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer")
@patch("ska_sdp_qa_metric_generator.plasma_common_processor.ska_sdp_config")
def test_exception(mock_config, mock_producer, monkeypatch):
    """Test the exception gets raised"""
    monkeypatch.setenv("SDP_PB_ID", "pb-test-123")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "01")

    with pytest.raises(ValueError, match="ENV 'SDP_KAFKA_HOST' is not set"):
        _ = SignalDisplayFake()
