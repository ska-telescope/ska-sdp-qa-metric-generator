"""Tests for the Plasma to Metrics class"""

# pylint: disable=abstract-class-instantiated,protected-access

import gzip
import logging
import os
from pathlib import Path
from unittest.mock import AsyncMock, call, patch

import msgpack
import msgpack_numpy
import pytest
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline
from ska_sdp_dataqueues import Encoding
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes

from ska_sdp_qa_metric_generator.plasma_to_metrics import SignalDisplayMetrics
from ska_sdp_qa_metric_generator.utils.config import ALL_METRICS
from tests.utils import untar

msgpack_numpy.patch()
PLASMA_SOCKET = "/tmp/plasma"
logger = logging.getLogger(__name__)

TEST_DATA = {
    "mid": {
        "calls": {
            "stats": 28,
            MetricDataTypes.SPECTRUM: 24,
            MetricDataTypes.PHASE: 24,
            MetricDataTypes.AMPLITUDE: 24,
            MetricDataTypes.LAG_PLOT: 24,
            MetricDataTypes.BAND_AVERAGED_X_CORR: 24,
            MetricDataTypes.UV_COVERAGE: 24,
        }
    },
    "low": {
        "calls": {
            "stats": 106,
            MetricDataTypes.SPECTRUM: 102,
            MetricDataTypes.PHASE: 102,
            MetricDataTypes.AMPLITUDE: 102,
            MetricDataTypes.LAG_PLOT: 102,
            MetricDataTypes.BAND_AVERAGED_X_CORR: 102,
            MetricDataTypes.UV_COVERAGE: 102,
        }
    },
}


def _get_data(telescope, metric_type):
    path = os.path.join("tests", "data", "tests", f"{telescope}.{metric_type}.gz")
    with gzip.open(path, mode="r") as infile:
        return msgpack.unpack(infile)


def _clean_data(metric_type: str, input_data: dict) -> dict:
    if metric_type == "stats":
        del input_data["data"]["time_since_last_payload"]
        del input_data["data"]["time"]
    else:
        del input_data["data"]["timestamp"]

    return input_data


def _validate_sent_data(telescope, metric_type, input_data):
    # convert to Python objects
    data = [_clean_data(metric_type, d) for d in input_data if "data" in d]
    correct_data = _get_data(telescope, metric_type)

    check_length = len(data) == len(correct_data)
    assert check_length, "Processed data doesn't match length check"
    check_sameness = data == correct_data
    if not check_sameness:
        for index, item in enumerate(data):
            check_item = item == correct_data[index]
            assert check_item, f"Item {index} doesn't match"
    assert check_sameness, "Processed data doesn't match"


# Disabling `mid` temporarily: "mid",
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "metric_type",
    ALL_METRICS + ["stats"],
)
@pytest.mark.parametrize("telescope", ["low"])
@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer", autospec=True)
async def test_complete(mock_producer, telescope: str, metric_type: str):
    """Run a simple test to check if things work"""

    producer_instance = AsyncMock()
    mock_producer.return_value = producer_instance

    path = f"tests/data/{telescope}.ms"
    logger.info("Untarring %s", telescope)
    if not Path(path).exists():
        input_ms_path = untar(f"{path}.tar.gz")
    else:
        input_ms_path = Path(path)
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=[metric_type]
    )
    metrics_processor._data_queue = producer_instance

    logger.info("Run processor...")
    # run plasma writer and processor til done
    await arun_emulated_sdp_pipeline(
        input_ms_path,
        PLASMA_SOCKET,
        20000000,
        metrics_processor,
        num_scans=1,
    )

    assert mock_producer.call_args_list == [
        call(server="fake_kafka", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]

    # Move asserts out of assert for performance when things get bad:

    made_calls = producer_instance.mock_calls
    made_calls_length = len(made_calls)
    assert made_calls_length == TEST_DATA[telescope]["calls"][metric_type]

    # convert all data to json:
    data = [c.kwargs for c in made_calls]
    _validate_sent_data(telescope, metric_type, data)


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer", autospec=True)
def test_if_all_metrics_works_correctly(mock_producer):
    """Check if the correct values are set for all stats requested"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=["all"]
    )
    assert metrics_processor.metrics == ALL_METRICS
    assert metrics_processor.enable_stats
    assert mock_producer.call_args_list == [
        call(server="fake_kafka", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer", autospec=True)
def test_no_metrics_requested(mock_producer):
    """Check if no metrics are requested"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=None
    )
    assert len(metrics_processor.metrics) == 0
    assert metrics_processor.enable_stats
    assert mock_producer.call_args_list == [
        call(server="fake_kafka", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer", autospec=True)
def test_only_stats_requested(mock_producer):
    """Check if no metrics are requested"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=["stats"]
    )
    assert len(metrics_processor.metrics) == 0
    assert metrics_processor.enable_stats
    assert mock_producer.call_args_list == [
        call(server="fake_kafka", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer", autospec=True)
def test_no_stats_requested(mock_producer):
    """Check if single metric requested that the stats is disabled"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False,
        ignore_config_db=True,
        disable_kafka=False,
        metrics=[MetricDataTypes.SPECTRUM],
    )
    assert metrics_processor.metrics == [MetricDataTypes.SPECTRUM]
    assert metrics_processor.enable_stats is False
    assert mock_producer.call_args_list == [
        call(server="fake_kafka", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]
