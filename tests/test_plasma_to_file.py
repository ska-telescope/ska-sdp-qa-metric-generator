"""Tests for the Plasma to File class"""

# pylint: disable=abstract-class-instantiated


from ska_sdp_qa_metric_generator.plasma_to_file import SignalDisplayMetricsFile
from ska_sdp_qa_metric_generator.utils.config import ALL_METRICS


def test_no_metrics_requested():
    """Check if no metrics are requested"""
    metrics_processor = SignalDisplayMetricsFile(metrics=None)
    assert metrics_processor.metrics == ALL_METRICS
