"""Test the functions in utilities"""

from datetime import date
from unittest.mock import patch

from ska_sdp_qa_metric_generator.utils.utilities import create_id


@patch("ska_sdp_qa_metric_generator.utils.utilities.date", autospec=True)
def test_create_id(mock_date):
    """Test the ID creation method"""
    mock_date.today.return_value = date(2024, 4, 1)

    assert create_id("pytest").startswith("pytest-test-20240401-")


@patch("ska_sdp_qa_metric_generator.utils.utilities.date", autospec=True)
def test_create_id_2(mock_date):
    """Test the ID creation method"""
    mock_date.today.return_value = date(2024, 4, 1)

    assert create_id("pytest", generator="next").startswith("pytest-next-20240401-")
