"""Tests for the base plasma processor."""

import os
from dataclasses import dataclass
from datetime import date
from time import sleep, time
from unittest.mock import MagicMock, call, patch

import pytest
from overrides import override
from ska_sdp_dataqueues import Encoding

from ska_sdp_qa_metric_generator.plasma_common_processor import SignalDisplayCommon
from ska_sdp_qa_metric_generator.utils.flow import SignalDisplayFlow

# pylint: disable=protected-access,unnecessary-dunder-call
SRC_FUNCTION = "ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics"


class SignalDisplayFake(SignalDisplayCommon):
    """Fake processor"""

    @staticmethod
    @override
    def create(argv) -> "SignalDisplayFake":  # pragma: no cover
        """Create instance of SignalDisplayMetrics."""
        parser = SignalDisplayCommon.create_parser("SignalDisplayMetrics", "")
        args = parser.parse_args(argv)

        return SignalDisplayFake(
            random_ids=args.use_random_ids,
            ignore_config_db=args.ignore_config_db,
            disable_kafka=args.disable_kafka,
            enable_stats=True,
        )


@dataclass
class ProcessingBlock:
    """Fake config return for processing block get."""

    eb_id: str


@dataclass
class ExecutionBlock:
    """Fake config return for execution block get."""

    subarray_id: str


@dataclass
class FlowKey:
    """Fake config return for flow keys get."""

    pb_id: str
    kind: str
    name: str


@dataclass
class Sink:
    """Fake config return for flow sink."""

    topics: str
    host: str
    format: str


@dataclass
class Source:
    """Fake config return for flow source."""

    function: str
    parameters: dict


@dataclass
class Flow:
    """Fake config return for flow get."""

    key: FlowKey
    sources: list[Source]
    sink: Sink


@pytest.fixture(scope="session", autouse=True)
def set_env():
    """Set this env for all tests"""
    os.environ["SDP_KAFKA_HOST"] = "fake_kafka"


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer")
@patch("ska_sdp_qa_metric_generator.plasma_common_processor.ska_sdp_config")
def test_init_basic(mock_config, mock_producer, monkeypatch):
    """Test the basic creation"""
    monkeypatch.setenv("SDP_PB_ID", "pb-test-123")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "01")
    processor = SignalDisplayFake()

    assert mock_config.mock_calls == [
        call.Config(),
        call.Config().txn(),
        call.Config().txn().__iter__(),
    ]
    assert mock_producer.mock_calls == [
        call(server="fake_kafka", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]

    assert processor.enable_stats is True
    assert processor.received_payloads == 0
    assert processor.received_time_slices == 0
    assert processor._processing_block_id == "pb-test-123"
    assert processor._execution_block_id == "eb-unknown"
    assert processor._subarray_id == "01"
    assert processor._scan_id == 0
    assert processor._disable_kafka is False
    assert processor._ignore_config_db is False
    assert processor.config == mock_config.Config.return_value
    assert processor._producer == mock_producer.return_value
    assert processor.time_since_last_payload <= time()


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer")
@patch("ska_sdp_qa_metric_generator.plasma_common_processor.ska_sdp_config")
def test_init_disable_network(mock_config, mock_producer):
    """Test the offline creation"""
    processor = SignalDisplayFake(random_ids=True, disable_kafka=True, ignore_config_db=True)

    assert mock_config.mock_calls == []
    assert mock_producer.mock_calls == []

    today = date.today().strftime("%Y%m%d")

    assert processor.enable_stats is True
    assert processor.received_payloads == 0
    assert processor.received_time_slices == 0
    assert processor._processing_block_id.startswith(f"pb-fake-{today}")
    assert processor._execution_block_id.startswith(f"eb-fake-{today}")
    assert processor._subarray_id == "01"
    assert processor._scan_id == 0
    assert processor._disable_kafka is True
    assert processor._ignore_config_db is True
    assert processor.config is None
    assert processor._producer is None
    assert processor.time_since_last_payload <= time()


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.DataQueueProducer")
@patch("ska_sdp_qa_metric_generator.plasma_common_processor.ska_sdp_config")
def test_init_env(mock_config, mock_producer, monkeypatch):
    """Test the basic creation with config DB access (most common creation)"""

    transaction = MagicMock()
    transaction.processing_block.get.return_value = ProcessingBlock(eb_id="eb-test-fake")
    transaction.execution_block.get.return_value = ExecutionBlock(subarray_id="99")

    mock_config.Config.return_value.txn.return_value.__iter__.return_value = [transaction]

    monkeypatch.setenv("SDP_PB_ID", "pb-test-123")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "01")
    processor = SignalDisplayFake()

    assert mock_config.mock_calls == [
        call.Config(),
        call.Config().txn(),
        call.Config().txn().__iter__(),
    ]
    assert mock_producer.mock_calls == [
        call(server="fake_kafka", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]
    assert transaction.mock_calls == [
        call.processing_block.get("pb-test-123"),
        call.execution_block.get("eb-test-fake"),
    ]

    assert processor.enable_stats is True
    assert processor.received_payloads == 0
    assert processor.received_time_slices == 0
    assert processor._processing_block_id == "pb-test-123"
    assert processor._execution_block_id == "eb-test-fake"
    assert processor._subarray_id == "99"
    assert processor._scan_id == 0
    assert processor._disable_kafka is False
    assert processor._ignore_config_db is False
    assert processor.config == mock_config.Config.return_value
    assert processor._producer == mock_producer.return_value
    assert processor.time_since_last_payload <= time()


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.ska_sdp_config")
def test_get_flows(mock_config, monkeypatch):
    """Test the basic creation with config DB access (most common creation)"""

    transaction = MagicMock()
    transaction.processing_block.get.return_value = ProcessingBlock(eb_id="eb-test-fake")
    transaction.execution_block.get.return_value = ExecutionBlock(subarray_id="99")

    flow_key = FlowKey(pb_id="pb-test-456", kind="data-queue", name="metrics-spectrum-01")

    transaction.flow.list_keys.return_value = [flow_key]
    transaction.flow.get.return_value = Flow(
        key=flow_key,
        sources=[
            Source(
                function=SRC_FUNCTION,
                parameters={
                    "metric_type": "spectrum",
                    "nchan_avg": 5,
                    "additional_windows": 5,
                    "rounding_sensitivity": 7,
                },
            )
        ],
        sink=Sink(topics="metrics-spectrum-01", host="kafka://host:port", format="msgpack_numpy"),
    )

    mock_config.Config.return_value.txn.return_value.__iter__.return_value = [transaction]

    monkeypatch.setenv("SDP_PB_ID", "pb-test-456")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "99")
    processor = SignalDisplayFake()
    assert processor._processing_block_id == "pb-test-456"
    assert processor._execution_block_id == "eb-test-fake"
    assert processor._subarray_id == "99"

    processor._flow_watcher(["spectrum"])
    sleep(1)

    assert processor.flows == {
        "spectrum": [
            SignalDisplayFlow(
                name="metrics-spectrum-01",
                topic="metrics-spectrum-01",
                host="kafka://host:port",
                data_format="msgpack_numpy",
                metric_type="spectrum",
                nchan_avg=5,
                additional_windows=5,
                rounding_sensitivity=7,
                flow=Flow(
                    key=FlowKey(
                        pb_id="pb-test-456", kind="data-queue", name="metrics-spectrum-01"
                    ),
                    sources=[
                        Source(
                            function=SRC_FUNCTION,
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 5,
                                "additional_windows": 5,
                                "rounding_sensitivity": 7,
                            },
                        )
                    ],
                    sink=Sink(
                        topics="metrics-spectrum-01",
                        host="kafka://host:port",
                        format="msgpack_numpy",
                    ),
                ),
                state=None,
            )
        ]
    }
    assert transaction.mock_calls == [
        call.processing_block.get("pb-test-456"),
        call.execution_block.get("eb-test-fake"),
        call.flow.list_keys(),
        call.flow.get(FlowKey(pb_id="pb-test-456", kind="data-queue", name="metrics-spectrum-01")),
    ]


@patch("ska_sdp_qa_metric_generator.plasma_common_processor.ska_sdp_config")
def test_get_flows_spectral_range(mock_config, monkeypatch):
    """Test the basic creation with config DB access (most common creation)"""

    transaction = MagicMock()
    transaction.processing_block.get.return_value = ProcessingBlock(eb_id="eb-test-fake")
    transaction.execution_block.get.return_value = ExecutionBlock(subarray_id="99")

    flow_key = FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01")

    transaction.flow.list_keys.return_value = [flow_key]
    transaction.flow.get.return_value = Flow(
        key=flow_key,
        sources=[
            Source(
                function=SRC_FUNCTION,
                parameters={
                    "metric_type": "spectrum",
                    "nchan_avg": 1,
                    "additional_windows": 1,
                    "rounding_sensitivity": 7,
                },
            )
        ],
        sink=Sink(topics="metrics-spectrum-01", host="kafka://host:port", format="msgpack_numpy"),
    )
    transaction.flow.state.return_value.get.return_value = {
        "windows": [{"start": 100, "end": 100, "channels_averaged": 1}]
    }

    mock_config.Config.return_value.txn.return_value.__iter__.return_value = [transaction]
    watcher_mock = MagicMock()
    watcher_mock.txn.return_value.__iter__.return_value = [transaction]
    mock_config.Config.return_value.watcher.return_value = [watcher_mock]

    monkeypatch.setenv("SDP_PB_ID", "pb-test-789")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "99")
    processor = SignalDisplayFake()
    assert processor._processing_block_id == "pb-test-789"
    assert processor._execution_block_id == "eb-test-fake"
    assert processor._subarray_id == "99"
    processor.config = None
    processor._flow_watcher(["spectrum"])
    sleep(1)

    assert processor.flows == {
        "spectrum": [
            SignalDisplayFlow(
                name="metrics-spectrum-01",
                topic="metrics-spectrum-01",
                host="kafka://host:port",
                data_format="msgpack_numpy",
                metric_type="spectrum",
                nchan_avg=1,
                additional_windows=1,
                rounding_sensitivity=7,
                flow=Flow(
                    key=FlowKey(
                        pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01"
                    ),
                    sources=[
                        Source(
                            function=SRC_FUNCTION,
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 1,
                                "additional_windows": 1,
                                "rounding_sensitivity": 7,
                            },
                        )
                    ],
                    sink=Sink(
                        topics="metrics-spectrum-01",
                        host="kafka://host:port",
                        format="msgpack_numpy",
                    ),
                ),
                state={"windows": [{"start": 100, "end": 100, "channels_averaged": 1}]},
            )
        ]
    }

    assert transaction.mock_calls == [
        call.processing_block.get("pb-test-789"),
        call.execution_block.get("eb-test-fake"),
        call.flow.list_keys(),
        call.flow.get(FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01")),
        call.flow.state(
            Flow(
                key=FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01"),
                sources=[
                    Source(
                        function=SRC_FUNCTION,
                        parameters={
                            "metric_type": "spectrum",
                            "nchan_avg": 1,
                            "additional_windows": 1,
                            "rounding_sensitivity": 7,
                        },
                    )
                ],
                sink=Sink(
                    topics="metrics-spectrum-01", host="kafka://host:port", format="msgpack_numpy"
                ),
            )
        ),
        call.flow.state().get(),
    ]
