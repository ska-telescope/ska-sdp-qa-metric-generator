#!/bin/env bash

set -e

default_url='https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-core/-/raw/main/data/AA05LOW.ms.tar.gz?inline=false'
default_file='AA05LOW.ms'

SAMPLE_DATA="${SAMPLE_DATA:-$default_url}"
SAMPLE_DATA_NAME="${SAMPLE_DATA_NAME:-$default_file}"
PROCESSOR='ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics'

extractData()
{
    printConfig
    mkdir -p /testdata
    wget -O /testdata/data.tar.gz "$1"
    cd /testdata
    tar xzf data.tar.gz
}


doNothing()
{
    trap : TERM; sleep infinity & wait
}

doSample()
{
    extractData "$SAMPLE_DATA"

    while true; do
        plasma-processor ska_sdp_qa_metric_generator.plasma_to_qa.SignalDisplay --input "$SAMPLE_DATA_NAME" --use-random-ids
        sleep 60
    done
}

doPlasma()
{
    extractData "$SAMPLE_DATA"
    extractData "https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-metric-generator.git/gitlab-lfs/objects/f262747c072b81ecfad7266d9f1c847f1c7da3ae38e31af8f30f48aa87be5367?inline=false"

    while true; do
        plasma-processor $PROCESSOR --input "$SAMPLE_DATA_NAME" --use-random-ids
        sleep 60
        plasma-processor $PROCESSOR --input "1651789539_sdp_l0_6ant_auto.ms" --use-random-ids
        sleep 60
    done
}

printConfig()
{
    echo "URL:  $SAMPLE_DATA"
    echo "FILE: $SAMPLE_DATA_NAME"
}

all()
{
  plasma-processor $PROCESSOR --metrics all
}

case $1 in
    nothing) doNothing ;;
    sample) doSample ;;
    plasma) doPlasma ;;
    config) printConfig ;;
    *) echo "Unknown operation" ;;
esac
