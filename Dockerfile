FROM artefact.skao.int/ska-build-python:0.1.1 AS builder

ENV POETRY_NO_INTERACTION=1
ENV POETRY_VIRTUALENVS_IN_PROJECT=1
ENV POETRY_VIRTUALENVS_CREATE=1

WORKDIR /src

COPY pyproject.toml poetry.lock install_measures.sh ./

RUN rm -rf /root/.casarc /usr/share/casacore/data \
    && ./install_measures.sh /usr/share/casacore/data

# Install dependencies
RUN poetry install --only main --no-root

FROM artefact.skao.int/ska-python:0.1.2

ENV VIRTUAL_ENV=/src/.venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV PYTHONPATH="$PYTHONPATH:/src/"
ENV PYTHONUNBUFFERED=1
ENV TZ=Etc/UTC

# User Setup
ARG USERNAME=signal_display
ARG USER_UID=1000
ARG USER_GID=1000

RUN groupadd --gid ${USER_GID} ${USERNAME} \
    && useradd -s /bin/bash --uid ${USER_UID} --gid ${USER_GID} -m ${USERNAME}

USER ${USERNAME}

COPY --from=builder /usr/share/casacore/data /usr/share/casacore/data
COPY --from=builder /root/.casarc /root/.casarc
COPY --from=builder ${VIRTUAL_ENV} ${VIRTUAL_ENV}

WORKDIR /src

COPY src/ska_sdp_qa_metric_generator ./ska_sdp_qa_metric_generator

COPY generator-helper.sh /generator-helper.sh
