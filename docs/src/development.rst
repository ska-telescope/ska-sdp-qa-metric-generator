Development
===========

For local development there is a poetry environment that will be able to install most of the
components, however some of the components assume you also have specific libraries installed on the
host machine (you can find these in the ``Dockerfile``). To work around this it is easier to use the
Docker image instead for testing.

The entrypoint into this application is into ``plasma_to_metrics.py``.

The current processor is built based on the plasma processors available `here
<https://developer.skao.int/projects/ska-sdp-realtime-receive-processors/en/latest/getting_started.html>`_.

Running the script
------------------

To run the script run the following command:

.. note:: You need to update the Kafka host and the MS file location.

.. code-block:: bash

    BROKER_INSTANCE=broker plasma-processor \
      ska_sdp_qa_metric_generator.plasma_to_metrics.SignalDisplayMetrics \
      --input <ms file>

There are also helper arguments that can be used:

.. role:: bash(code)
   :language: bash

.. list-table::
  :widths: 25 75
  :header-rows: 1

  * - Argument
    - Description
  * - :bash:`--use-random-ids`
    - This will generate random IDs instead of trying to read them from the enviroment.
  * - :bash:`--ignore-config-db`
    - This instructs not to try to read the config DB.
  * - :bash:`--disable-kafka`
    - This will run the process, but not output to Kafka.
  * - :bash:`--metrics <all,stats,spectrum,phase,amplitude,lag>`
    - This specifies a comma seperated list of metrics to generate, using ``all`` will generate everything.

For a list of other commands and options available use ``--help`` when running the script.
