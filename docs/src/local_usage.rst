Usage in Local Testing
======================

For local testing it is simplest within the docker container. However, it is possible to run on your
host machine, refer to ``Dockerfile`` for the required libraries to be installed on your host
machine.

To be able to use the helper commands it is assumed that you have the `Signal Metric API <https://developer.skao.int/projects/ska-sdp-qa-data-api/en/latest/>`_ locally and already running.

You then need to build the Docker environment using:

.. code-block:: bash

    make build

Then to run system copy a MS directory into ``data/``. And run the
following:

.. code-block:: bash

    make run MS=data/<name of MS file>

The processor will then send the data through to Kafka that is assumed to be running.
